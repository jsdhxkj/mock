package cn.com.random;

import cn.mock.random.*;
import org.junit.Test;

/**
 * @author JiaZH
 * @date 2024-01-12
 */
public class RandomTest {

    @Test
    public void testBasic() {
        Boolean aBoolean = RandomBasic.getBoolean(1, 8, true);
        System.out.println("aBoolean = " + aBoolean);
        Integer natural = RandomBasic.getNatural(1, 10);
        System.out.println("natural = " + natural);
        Character lower = RandomBasic.getCharacter("lower");
        System.out.println("lower = " + lower);
        Double aDouble = RandomBasic.getDouble(1, 50, 2, 5);
        System.out.println("aDouble = " + aDouble);
        String str = RandomBasic.getStr();
        System.out.println("str = " + str);
        Integer[] range = RandomBasic.getRange(1, 10, 1);
        for (Integer i : range) {
            System.out.println("range i = " + i);
        }
    }

    @Test
    public void testRandomDate() {
        String date = RandomDate.getDate("");
        System.out.println("date = " + date);
        String time = RandomDate.getTime("");
        System.out.println("time = " + time);
        String dateTime = RandomDate.getDateTime("");
        System.out.println("dateTime = " + dateTime);
        String now = RandomDate.getNow("", "");
        System.out.println("now = " + now);
        System.out.println("now year = " + RandomDate.getNow("year", ""));
        System.out.println("now month = " + RandomDate.getNow("month", ""));
        System.out.println("now day = " + RandomDate.getNow("day", ""));
        System.out.println("now hour = " + RandomDate.getNow("hour", ""));
        System.out.println("now minute = " + RandomDate.getNow("minute", ""));
        System.out.println("now second = " + RandomDate.getNow("second", ""));
        System.out.println("now week = " + RandomDate.getNow("week", ""));
    }

    @Test
    public void testRandomImage() {
        String image = RandomImage.getImage("d45xad2", "596495", "dasf26", "png", "41564156456");
        System.out.println("image = " + image);
        String dataImage = RandomImage.getDataImage();
        System.out.println("dataImage = " + dataImage);
    }

    @Test
    public void testRandomColor() {
        String green = RandomColor.getColor("green");
        System.out.println("green = " + green);
        String hex = RandomColor.getHex();
        System.out.println("hex = " + hex);
        String rgb = RandomColor.getRgb();
        System.out.println("rgb = " + rgb);
        String rgba = RandomColor.getRgba();
        System.out.println("rgba = " + rgba);
        String hsl = RandomColor.getHsl();
        System.out.println("hsl = " + hsl);
    }

    @Test
    public void testRandomText() {
        String paragraph = RandomText.getParagraph(null, null);
        System.out.println("paragraph = " + paragraph);
        String cparagraph = RandomText.getCparagraph(null, null);
        System.out.println("cparagraph = " + cparagraph);
        String sentence = RandomText.getSentence(null, null);
        System.out.println("sentence = " + sentence);
        String csentence = RandomText.getCsentence(null, null);
        System.out.println("csentence = " + csentence);
        String word = RandomText.getWord(null, null);
        System.out.println("word = " + word);
        String cword = RandomText.getCword(null, null, null);
        System.out.println("cword = " + cword);
        String title = RandomText.getTitle(null, null);
        System.out.println("title = " + title);
        String ctitle = RandomText.getCtitle(null, null);
        System.out.println("ctitle = " + ctitle);
    }

    @Test
    public void testRandomName() {
        String first = RandomName.getFirst();
        System.out.println("first = " + first);
        String last = RandomName.getLast();
        System.out.println("last = " + last);
        String name = RandomName.getName(true);
        System.out.println("name = " + name);
        String cfirst = RandomName.getCfirst();
        System.out.println("cfirst = " + cfirst);
        String clast = RandomName.getClast();
        System.out.println("clast = " + clast);
        String cname = RandomName.getCname();
        System.out.println("cname = " + cname);
    }


    @Test
    public void testRandomWeb() {
        String url = RandomWeb.getUrl();
        System.out.println("url = " + url);
        String url1 = RandomWeb.getUrl("ftp", "125.26.46.95");
        System.out.println("url1 = " + url1);
        String protocol = RandomWeb.getProtocol();
        System.out.println("protocol = " + protocol);
        String domain = RandomWeb.getDomain();
        System.out.println("domain = " + domain);
        String domain1 = RandomWeb.getDomain("cn");
        System.out.println("domain1 = " + domain1);
        String tld = RandomWeb.getTld();
        System.out.println("tld = " + tld);
        String email = RandomWeb.getEmail("qq.com");
        System.out.println("email = " + email);
        String email1 = RandomWeb.getEmail();
        System.out.println("email1 = " + email1);
        String ip = RandomWeb.getIp();
        System.out.println("ip = " + ip);
    }

    @Test
    public void testRandomAddress() {
        String region = RandomAddress.getRegion();
        System.out.println("region = " + region);
        String province = RandomAddress.getProvince();
        System.out.println("province = " + province);
        String city = RandomAddress.getCity();
        System.out.println("city = " + city);
        String county = RandomAddress.getCounty();
        System.out.println("county = " + county);
        String address = RandomAddress.getAddress();
        System.out.println("address = " + address);
    }

    @Test
    public void testRandomMisc() {
        String idCard = RandomMisc.getIdCard();
        System.out.println("idCard = " + idCard);
        String uuid = RandomMisc.getUuid();
        System.out.println("uuid = " + uuid);
    }
}

package cn.mock.enums;

import java.util.Objects;

/**
 * @author JiaZH
 * @date 2024-01-12
 */
public enum Colors {

    navy("#001F3F", "#000080"),
    blue("#0074D9", "#0000ff"),
    aqua("#7FDBFF", "#00ffff"),
    teal("#39CCCC", "#008080"),
    olive("#3D9970", "#008000"),
    green("#2ECC40", "#008000"),
    lime("#01FF70", "#00ff00"),
    yellow("#FFDC00", "#ffff00"),
    orange("#FF851B", "#ffa500"),
    red("#FF4136", "#ff0000"),
    maroon("#85144B", "#800000"),
    fuchsia("#F012BE", "#ff00ff"),
    purple("#B10DC9", "#800080"),
    silver("#DDDDDD", "#c0c0c0"),
    gray("#AAAAAA", "#808080"),
    black("#111111", "#000000"),
    white("#FFFFFF", "#FFFFFF"),
    ;

    private static Colors[] values;

    public static Colors getByName(String name) {
        if (Objects.isNull(values)) {
            values = values();
        }
        for (Colors value : values) {
            if (value.name().equalsIgnoreCase(name)) {
                return value;
            }
        }
        return null;
    }

    Colors(String nicer, String value) {
        this.nicer = nicer;
        this.value = value;
    }

    private final String nicer;

    private final String value;

    public String getNicer() {
        return nicer;
    }

    public String getValue() {
        return value;
    }
}

package cn.mock.enums;

import cn.mock.constant.Constant;

import java.util.Objects;

/**
 * @author JiaZH
 * @date 2024-01-15
 */
public enum CharacterPool {

    LOWER(Constant.LOWER),
    UPPER(Constant.UPPER),
    NUMBER(Constant.NUMBER),
    SYMBOL(Constant.SYMBOL),
    ALPHA(Constant.LOWER + Constant.UPPER),
    ALL(Constant.LOWER + Constant.UPPER + Constant.NUMBER + Constant.SYMBOL);

    public static String getPool(String pool) {
        if (Objects.isNull(values)) {
            values = values();
        }
        for (CharacterPool value : values) {
            if (value.name().equalsIgnoreCase(pool)) {
                return value.getPool();
            }
        }
        return pool;
    }

    private static CharacterPool[] values;

    CharacterPool(String pool) {
        this.pool = pool;
    }

    private final String pool;

    public String getPool() {
        return pool;
    }
}

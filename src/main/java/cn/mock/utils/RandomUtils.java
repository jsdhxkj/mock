package cn.mock.utils;

import cn.mock.random.RandomBasic;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author JiaZH
 * @date 2024-01-12
 */
public class RandomUtils {

    public static String pick(String[] arr, Integer min, Integer max) {
        if (Objects.isNull(arr)) {
            arr = new String[0];
            min = 1;
            max = 1;
        } else {
            min = ObjectUtils.defaultIfNull(min, 1);
            max = ObjectUtils.defaultIfNull(max, min);
        }

        if (min == 1 && max == 1) {
            return arr[RandomBasic.getNatural(0, arr.length - 1)];
        }
        return shuffle(arr, min, max);
    }


    public static String pick(String[] arr) {
        return pick(arr, null, null);
    }

    public static String pick(List<String> arr, Integer min, Integer max) {
        if (Objects.isNull(arr)) {
            arr = new ArrayList<>();
            min = 1;
            max = 1;
        } else {
            min = ObjectUtils.defaultIfNull(min, 1);
            max = ObjectUtils.defaultIfNull(max, min);
        }
        if (min == 1 && max == 1) {
            return arr.get(RandomBasic.getNatural(0, arr.size() - 1));
        }
        return shuffle(arr, min, max);
    }

    public static String pick(List<String> arr) {
        return pick(arr, null, null);
    }

    public static String shuffle(String[] arr, Integer min, Integer max) {
        if (Objects.isNull(arr)) {
            arr = new String[0];
            min = 1;
            max = 1;
        } else {
            min = ObjectUtils.defaultIfNull(min, 1);
            max = ObjectUtils.defaultIfNull(max, min);
        }
        ArrayUtils.shuffle(arr);
        return arr[RandomBasic.getNatural(min, max)];
    }

    public static String shuffle(List<String> arr, Integer min, Integer max) {
        if (Objects.isNull(arr)) {
            arr = new ArrayList<>();
            min = 1;
            max = 1;
        } else {
            min = ObjectUtils.defaultIfNull(min, 1);
            max = ObjectUtils.defaultIfNull(max, min);
        }
        Collections.shuffle(arr);
        return arr.get(RandomBasic.getNatural(min, max));
    }

}

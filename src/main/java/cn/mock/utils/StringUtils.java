package cn.mock.utils;

/**
 * @author JiaZH
 * @date 2024-01-12
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    public static String firstUpper(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

}

package cn.mock.utils;

import cn.mock.random.RandomName;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

/**
 * @author JiaZH
 * @date 2024-01-15
 */
public class FileUtils {

    public static List<String> readAllLines(String path) {
        try {
            return Files.readAllLines(Paths.get(Objects.requireNonNull(RandomName.class.getResource(path)).toURI()));
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

}

package cn.mock.random;

import cn.mock.utils.FileUtils;
import cn.mock.utils.RandomUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JiaZH
 * @date 2024-01-15
 */
public class RandomName {

    private static final List<String> MALE;
    private static final List<String> FEMALE;

    private static final List<String> ENGLISH_SURNAME = new ArrayList<>();
    private static final List<String> ENGLISH_NAME;
    private static final List<String> CHINESE_SURNAME;
    private static final List<String> CHINESE_NAME;

    static {
        MALE = FileUtils.readAllLines("/male.txt");
        FEMALE = FileUtils.readAllLines("/female.txt");
        ENGLISH_NAME = FileUtils.readAllLines("/english_name.txt");
        CHINESE_SURNAME = FileUtils.readAllLines("/chinese_surname.txt");
        CHINESE_NAME = FileUtils.readAllLines("/chinese_name.txt");
        ENGLISH_SURNAME.addAll(MALE);
        ENGLISH_SURNAME.addAll(FEMALE);
    }

    public static String getFirst() {
        return RandomUtils.pick(ENGLISH_SURNAME);
    }

    public static String getLast() {
        return RandomUtils.pick(ENGLISH_NAME);
    }

    public static String getName(Boolean middle) {
        return getFirst() + " " + (middle ? getFirst() + " " : "") + getLast();
    }

    public static String getCfirst() {
        return RandomUtils.pick(CHINESE_SURNAME);
    }

    public static String getClast() {
        return RandomUtils.pick(CHINESE_NAME);
    }

    public static String getCname() {
        return getCfirst() + getClast();
    }

}

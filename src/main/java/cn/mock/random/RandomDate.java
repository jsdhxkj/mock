package cn.mock.random;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author JiaZH
 * @date 2024-01-12
 */
public class RandomDate {

    private static final String YYYY_MM_DD = "yyyy-MM-dd";

    private static final String HH_MM_SS = "HH:mm:ss";

    private static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    private static Date randomDate(Date min, Date max) {
        min = ObjectUtils.defaultIfNull(min, new Date(0));
        max = ObjectUtils.defaultIfNull(max, new Date());
        long time = (long) (Math.random() * (max.getTime() - min.getTime()));
        return new Date(time);
    }

    private static Date randomDate() {
        return randomDate(null, null);
    }

    /**
     * 返回一个随机的日期字符串。
     *
     * @param format
     * @return
     */
    public static String getDate(String format) {
        format = StringUtils.defaultIfBlank(format, YYYY_MM_DD);
        return DateFormatUtils.format(randomDate(), format);
    }

    /**
     * 返回一个随机的时间字符串。
     *
     * @param format
     * @return
     */
    public static String getTime(String format) {
        format = StringUtils.defaultIfBlank(format, HH_MM_SS);
        return DateFormatUtils.format(randomDate(), format);
    }

    /**
     * 返回一个随机的日期和时间字符串。
     *
     * @param format
     * @return
     */
    public static String getDateTime(String format) {
        format = StringUtils.defaultIfBlank(format, YYYY_MM_DD_HH_MM_SS);
        return DateFormatUtils.format(randomDate(), format);
    }

    /**
     * 返回当前的日期和时间字符串。
     *
     * @param unit year|month|day|hour|minute|second|week
     * @param format yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getNow(String unit, String format) {
        unit = StringUtils.defaultIfBlank(unit, "").toLowerCase();
        format = StringUtils.defaultIfBlank(format, YYYY_MM_DD_HH_MM_SS);
        if (StringUtils.isBlank(unit) && !Pattern.compile("year|month|day|hour|minute|second|week").matcher(unit.toLowerCase()).find()) {
            unit = "";
        }
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        switch (unit) {
            case "year":
                calendar.set(Calendar.MONTH, 0);
            case "month":
                calendar.set(Calendar.DAY_OF_MONTH, 0);
            case "week":
            case "day":
                calendar.set(Calendar.HOUR_OF_DAY, 0);
            case "hour":
                calendar.set(Calendar.MINUTE, 0);
            case "minute":
                calendar.set(Calendar.SECOND, 0);
            case "second":
                calendar.set(Calendar.MILLISECOND, 0);
        }
        if ("week".equals(unit)) {
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - calendar.get(Calendar.DAY_OF_MONTH));
        }
        return DateFormatUtils.format(calendar.getTime(), format);
    }

}

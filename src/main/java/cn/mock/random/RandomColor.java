package cn.mock.random;

import cn.mock.enums.Colors;
import cn.mock.utils.ColorUtils;
import cn.mock.utils.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.RoundingMode;
import java.util.Objects;

/**
 * @author JiaZH
 * @date 2024-01-12
 */
public class RandomColor {

    public static String getColor(String name) {
        if (StringUtils.isNotBlank(name)) {
            Colors dict = Colors.getByName(name);
            if (Objects.nonNull(dict)) {
                return dict.getNicer();
            }
        }
        return getHex();
    }

    public static String getHex() {
        Integer[] hsv = goldenRatioColor();
        Integer[] rgb = ColorUtils.hsv2rgb(hsv);
        return ColorUtils.rgb2hex(rgb[0], rgb[1], rgb[2]);
    }

    public static String getRgb() {
        Integer[] hsv = goldenRatioColor();
        Integer[] rgb = ColorUtils.hsv2rgb(hsv);
        return "rgb" + "(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
    }

    public static String getRgba() {
        Integer[] hsv = goldenRatioColor();
        Integer[] rgb = ColorUtils.hsv2rgb(hsv);
        double random = Math.random();
        random = NumberUtils.toScaledBigDecimal(random).setScale(2, RoundingMode.HALF_UP).doubleValue();
        return "rgba(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + "," + random + ")";
    }

    public static String getHsl() {
        Integer[] hsv = goldenRatioColor();
        Integer[] hsl = ColorUtils.hsv2hsl(hsv);
        return "hsl(" + hsl[0] + "," + hsl[1] + "," + hsl[2] + ")";
    }

    private static Integer[] goldenRatioColor() {
        double goldenRatio = 0.618033988749895;
        double hue = Math.random();
        hue += goldenRatio;
        hue %= 1;
        double saturation = 0.5;
        double value = 0.95;
        return new Integer[]{(int) (hue * 360), (int) (saturation * 100), (int) (value * 100)};
    }

}

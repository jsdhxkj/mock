package cn.mock.random;

import cn.mock.constant.Constant;
import cn.mock.enums.CharacterPool;
import cn.mock.utils.StringUtils;
import org.apache.commons.lang3.ObjectUtils;

import java.util.StringJoiner;

/**
 * @author JiaZH
 * @date 2024-01-12
 */
public class RandomText {

    /**
     * 随机生成一段文本。
     * @param min
     * @param max
     * @return
     */
    public static String getParagraph(Integer min, Integer max) {
        int len = getLen(min, max);
        StringJoiner joiner = new StringJoiner(" ");
        for (int i = 0; i < len; i++) {
            joiner.add(getSentence(null, null));
        }
        return joiner.toString();
    }

    /**
     * 随机生成一段文本。
     * @return
     */
    public static String getParagraph() {
        return getParagraph(null, null);
    }

    /**
     * 随机生成一段中文文本。
     * @param min
     * @param max
     * @return
     */
    public static String getCparagraph(Integer min, Integer max) {
        int len = getLen(min, max);
        StringJoiner joiner = new StringJoiner("");
        for (int i = 0; i < len; i++) {
            joiner.add(getCsentence(null, null));
        }
        return joiner.toString();
    }

    /**
     * 随机生成一段中文文本。
     * @return
     */
    public static String getCparagraph() {
        return getCparagraph(null, null);
    }

    /**
     * 随机生成一个句子，第一个单词的首字母大写。
     * @param min
     * @param max
     * @return
     */
    public static String getSentence(Integer min, Integer max) {
        int len = getLen(min, max);
        StringJoiner joiner = new StringJoiner(" ");
        for (int i = 0; i < len; i++) {
            joiner.add(getWord(null, null));
        }
        return StringUtils.firstUpper(joiner.toString()) + ".";
    }

    /**
     * 随机生成一个句子，第一个单词的首字母大写。
     * @return
     */
    public static String getSentence() {
        return getSentence(null, null);
    }

    /**
     * 随机生成一个中文句子。
     * @param min
     * @param max
     * @return
     */
    public static String getCsentence(Integer min, Integer max) {
        int len = getLen(min, max);
        StringJoiner joiner = new StringJoiner("");
        for (int i = 0; i < len; i++) {
            joiner.add(getCword(null, null, null));
        }
        return joiner + "。";
    }

    /**
     * 随机生成一个中文句子。
     * @return
     */
    public static String getCsentence() {
        return getCsentence(null, null);
    }

    /**
     * 随机生成一个单词。
     * @param min
     * @param max
     * @return
     */
    public static String getWord(Integer min, Integer max) {
        int len = getLen(min, max);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            builder.append(RandomBasic.getCharacter(CharacterPool.LOWER.name()));
        }
        return builder.toString();
    }

    /**
     * 随机生成一个单词。
     * @return
     */
    public static String getWord() {
        return getWord(null, null);
    }

    /**
     * 随机生成一个或多个汉字。
     * @param pool
     * @param min
     * @param max
     * @return
     */
    public static String getCword(String pool, Integer min, Integer max) {
        pool = StringUtils.defaultIfBlank(pool, Constant.DICT_KANJI);
        int len = getLen(min, max);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            builder.append(pool.charAt(RandomBasic.getNatural(0, pool.length() - 1)));
        }
        return builder.toString();
    }

    /**
     * 随机生成一个或多个汉字。
     * @param min
     * @param max
     * @return
     */
    public static String getCword(Integer min, Integer max) {
        return getCword(null, min, max);
    }

    /**
     * 随机生成一个或多个汉字。
     * @return
     */
    public static String getCword() {
        return getCword(null, null, null);
    }

    /**
     * 随机生成一句标题，其中每个单词的首字母大写。
     * @param min
     * @param max
     * @return
     */
    public static String getTitle(Integer min, Integer max) {
        int len = getLen(min, max);
        StringJoiner joiner = new StringJoiner(" ");
        for (int i = 0; i < len; i++) {
            joiner.add(StringUtils.firstUpper(getWord(null, null)));
        }
        return joiner.toString();
    }

    /**
     * 随机生成一句标题，其中每个单词的首字母大写。
     * @return
     */
    public static String getTitle() {
        return getTitle(null, null);
    }

    /**
     * 随机生成一句中文标题。
     * @param min
     * @param max
     * @return
     */
    public static String getCtitle(Integer min, Integer max) {
        int len = getLen(min, max);
        StringJoiner joiner = new StringJoiner("");
        for (int i = 0; i < len; i++) {
            joiner.add(getCword(null, null, null));
        }
        return joiner.toString();
    }

    /**
     * 随机生成一句中文标题。
     * @return
     */
    public static String getCtitle() {
        return getCtitle(null, null);
    }

    private static int getLen(Integer min, Integer max) {
        min = ObjectUtils.defaultIfNull(min, 3);
        max = ObjectUtils.defaultIfNull(max, 7);
        return RandomBasic.getNatural(min, max);
    }
}

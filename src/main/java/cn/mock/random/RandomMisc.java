package cn.mock.random;

import cn.mock.enums.CharacterPool;
import cn.mock.utils.FileUtils;
import cn.mock.utils.RandomUtils;

import java.util.List;
import java.util.UUID;

/**
 * @author JiaZH
 * @date 2024-01-15
 */
public class RandomMisc {

    private static final List<String> COUNTY_CODE;

    static {
        COUNTY_CODE = FileUtils.readAllLines("/county_code.txt");
    }

    /**
     * 获取一个UUID
     * @return
     */
    public static String getUuid() {
        return UUID.randomUUID().toString();
    }

    /**
     * 获取一个身份证号码
     * @return
     */
    public static String getIdCard() {
        int sum = 0;
        String id;
        Integer[] rank = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
        String[] last = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};

        id = RandomUtils.pick(COUNTY_CODE) +
                RandomDate.getDate("yyyyMMdd") +
                RandomBasic.getString(CharacterPool.NUMBER.name(), 3);


        for (int i = 0; i < id.length(); i++) {
            sum += id.charAt(i) * rank[i];
        }

        id += last[sum % 11];

        return id;
    }

}

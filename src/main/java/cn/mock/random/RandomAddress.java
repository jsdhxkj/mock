package cn.mock.random;

import cn.mock.utils.FileUtils;
import cn.mock.utils.RandomUtils;
import org.apache.commons.lang3.ObjectUtils;

import java.util.List;

/**
 * @author JiaZH
 * @date 2024-01-15
 */
public class RandomAddress {

    private static final List<String> PROVINCES;
    private static final List<String> CITIES;
    private static final List<String> COUNTY;

    private static final List<String> PROVINCE_CITIES;
    private static final List<String> ADDRESS_FLAGS;

    static {
        PROVINCES = FileUtils.readAllLines("/province.txt");
        CITIES = FileUtils.readAllLines("/cities.txt");
        COUNTY = FileUtils.readAllLines("/county.txt");
        PROVINCE_CITIES = FileUtils.readAllLines("/province_cities.txt");
        ADDRESS_FLAGS = FileUtils.readAllLines("/address_flags.txt");
    }

    /**
     * 随机生成一个大区。
     * @return
     */
    public static String getRegion() {
        String region = "东北,华北,华东,华中,华南,西南,西北";
        return RandomUtils.pick(region.split(","));
    }

    /**
     * 随机生成一个（中国）省（或直辖市、自治区、特别行政区）。
     * @return
     */
    public static String getProvince() {
        return RandomUtils.pick(PROVINCES);
    }

    /**
     * 随机生成一个（中国）市。
     * @return
     */
    public static String getCity() {
        return RandomUtils.pick(PROVINCE_CITIES);
    }

    /**
     * 随机生成一个（中国）县。
     * @return
     */
    public static String getCounty() {
        return RandomUtils.pick(COUNTY);
    }

    /**
     * 随机生成一个（中国）地址
     * @return
     */
    public static String getAddress() {
        StringBuilder result = new StringBuilder();

        //省市
        String provinceCity = RandomUtils.pick(PROVINCE_CITIES);
        double chance01 = Math.random();

        //去掉省
        if (chance01 < 0.2) {
            provinceCity = provinceCity.replaceFirst("省", "");
            //去掉市
        } else if (chance01 < 0.4) {
            provinceCity = provinceCity.replaceFirst("市", "");
            //去掉省和市
        } else if (chance01 < 0.6) {
            provinceCity = provinceCity.replaceFirst("省", "").replaceFirst("市", "");
            //没有省,直接取xxx市
        } else if (chance01 < 0.8) {
            provinceCity = RandomUtils.pick(CITIES);
            //省市都没有
        } else {
            provinceCity = "";
        }

        result.append(provinceCity);

        double chance02 = Math.random();
        //xxx路xxx号xxx小区xxx单元xxx室
        if (chance02 < 0.2) {
            result.append(RandomText.getCword(2, 3)).append("路")
                    .append(RandomBasic.getNatural(1, 8000)).append("号")
                    .append(RandomText.getCword(2, 4)).append("小区")
                    .append(RandomBasic.getNatural(1, 20)).append("单元")
                    .append(RandomBasic.getNatural(101, 2500)).append("室");
            //xxx路xxx地标
        } else if (chance02 < 0.4) {
            result.append(RandomText.getCword(2, 3)).append("路")
                    .append(RandomText.getCword(2, 4)).append(RandomUtils.pick(ADDRESS_FLAGS));
            //xxx路xxx号
        } else if (chance02 < 0.6) {
            result.append(RandomText.getCword(2, 3)).append("路")
                    .append(RandomBasic.getNatural(1, 8000)).append("号");
            //xxx路与xxx路交叉口
        } else if (chance02 < 0.8) {
            result.append(RandomText.getCword(2, 3)).append("路")
                    .append(RandomText.getCword(2, 3)).append("路")
                    .append("交叉口");
            //xxx街道xxx号
        } else {
            result.append(RandomText.getCword(2, 3)).append("街道")
                    .append(RandomBasic.getNatural(1, 8000)).append("号");
        }
        return result.toString();
    }

    /**
     * 随机生成一个邮政编码（六位数字）。
     * @param len
     * @return
     */
    public static String getZip(Integer len) {
        len = ObjectUtils.defaultIfNull(len, 6);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            builder.append(RandomBasic.getNatural(0, 9));
        }
        return builder.toString();
    }

    /**
     * 随机生成一个邮政编码（六位数字）。
     * @return
     */
    public static String getZip() {
        return getZip(null);
    }

}
